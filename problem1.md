# Problem1

Created: August 18, 2023 6:20 PM

Given the number of villagers `num_villagers` and a list of lists `attendees_lists` that record the attendees each evening, where 0 is the minstrel and all other numbers are the villagers, complete the function `total_song_knowers` to calculate the total number of villagers that know all the songs.

problem: keep track of total_song_knowers.

facts

- If minstrel present - adds new song and all attendees know_new_song
- if menstrel not present, all attendees learn all songs
- any previous attendee not present when menstal present, does not know every song (because menstrel introduced new song)
- only attendee that knows all songs and is present when minstrel present, now knows all songs
- only if attendee that was present with minstrel and knows all songs is present does all attendee know all songs

PseudCode:

input:

number_of_evenings: [[lists of ev_atts]...]

num_villagers as list of villagers(e.g villagers=[1,2,3,4,5…num_villagers])

attendees_list: record of attendees each evening

Output: know_all_songs

per loop: villagers who know all the songs

Variables needed for process:

know_all_songs

know_new_songs

know_all_songs_minus_new_song

Process:

loop through number_of_evenings

if start: no 0 and all attendees at start()   - add to total_song_knowers

else

**loop through list of attendee_list**

if minstrel (0) in attendee_list
    add attendee list to know_new_song dict with key as song#
    increase new_song by 1
    test if attendee in know_all_song and not in attendee list,
        remove from know all song
else
    loop through attendee list (ev_atts)
    if attendee  in tot_song_knowers
        add all attendees to total_song_knowers

    if no attendees in total_song_knowers
        check each attendee against know_new_songs
            if all new_songs are present among attendees, add all attendee to know all songs

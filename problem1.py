# def total_song_knowers(num_villagers,attendees_lists):
def total_song_knowers(num_villagers, attendees_lists):
    new_song = 1
    know_all_songs = []
    know_new_song = dict() # {song#:[ev_atts[x]]}
    know_all_songs_minus_new_song = []
    for ev_atts in attendees_lists:
        if 0 in ev_atts:
            know_new_song.update({new_song:ev_atts})
            new_song+=1
            # check if villager in know_all_songs not in ev_atts remove from know_all_songs
            if len(know_all_songs) > 0:
                for villager in range(1,num_villagers+1):
                    if villager in know_all_songs and villager not in ev_atts:
                        know_all_songs.remove(villager)
        else:
            count = 0
            # loop through attendee list
            new_songs_present = []
            while count < len(ev_atts):
                if ev_atts[count] in know_all_songs:
                    know_all_songs.extend(ev_atts)
                    count = len(ev_atts)
                    #print('know all:', know_all_songs)
                    #print('if count',count)
                else:
                    #print('else count',count)
                    for key,values in know_new_song.items():
                        if ev_atts[count] in values:
                            new_songs_present.append(key)
                            #print('new_songs_present',set(new_songs_present))
                    if set(new_songs_present) == set(know_new_song.keys()):
                        know_all_songs.extend(ev_atts)
                        count = len(ev_atts)
                    else:
                        count+=1
    result = set(know_all_songs )
    return len(result)


print(total_song_knowers(5,[[2, 4],[0, 1, 2],[1, 3, 4],[1, 2],[0, 5,],[3, 5]]))
# print(total_song_knowers(3, [[0, 1], [2, 3], [3, 1, 4]]))
# print(total_song_knowers(4, [[0, 2], [1, 0], [1, 0, 3, 4]]))
# print(total_song_knowers(7, [[0, 2, 4, 3], [4, 5], [5, 6, 7], [5, 1], [1, 5, 7, 0]]))
# evenings = [[2, 4],[0, 1, 2],[1, 3, 4],[1, 2],[0, 5,],[3, 5]]
# num_villagers = 5

# 3 [[0, 1], [1, 2, 3], [3, 1, 0]]
# 4 [[0, 2], [1, 0], [1, 0, 3, 4]],[2,1]
# 7 [[0, 2, 4, 3], [4, 5], [5, 6, 7], [5, 1], [1, 5, 7, 0]]
# Evening #	Attendees	Who knows all the songs	Reason
# 1	2, 4	2, 4	2 and 4 know all the songs
# 2	0, 1, 2	2	Only 2 knows all the songs sung
# 3	1, 3, 4	1, 2, 3, 4	1 knows the song from evening 2, and four knows the songs from evening 1, sharing them they now know all the songs
# 4	1, 2	1, 2, 3, 4	Nothing changes
# 5	0, 5		5 now knows a new song that no one else knows and doesn't know any other songs
# 6	3, 5	3, 5	3 and 5 share their songs and now know all of them
